// создание контроллера для главной страницы (поиск фильмов и расписание сеансов)
angular.module('app')
    .controller('MainCtrl', ['$scope', 'Genre', 'Film', function($scope, Genre, Film) {
        // Установка списка городов
        $scope.cities = [
            {
                id: 490,
                title: 'Калининград'
            },
            {
                id: 5841,
                title: 'Пионерск'
            },
            /*
            База данных по Зеленоградску не полная (Нет жанров и кинотеатров)
            {
                id: 6401,
                title: 'Зеленоград'
            },*/
            {
                id: 6241,
                title: 'Советск'
            }
        ];

        $scope.cityID = 490; // Установка города по умолчанию для поиска
        $scope.genreID = -1; // Установка поиска без жанра

        // Изменение города для поиска
        $scope.setCity = function (cityID) {
            $scope.cityID = cityID;
        };

        // Изменение жанра для поиска
        $scope.setGenre = function (genreID) {
            $scope.genreID = $scope.genreID == genreID ? -1 : genreID;
        };

        var onParamGenreID = function () {
            Film.FindToday($scope.cityID, $scope.genreID).then(function (films) {
                $scope.films = films;
            });
        };

        // Если был изменен город, обновляем список фильмов
        $scope.$watch('cityID', function () {
            $scope.genreID = -1;
            // Поиск всех жанров по городу
            Genre.FindAll($scope.cityID).then(function (genres) {
                $scope.genres = genres;
            });

            if ($scope.genreID == -1) {
                onParamGenreID();
            }
            else {
                $scope.genreID == -1
            }

        });
        // Если был изменен жанр, обновляем список фильмов
        $scope.$watch('genreID', onParamGenreID);

    }]);