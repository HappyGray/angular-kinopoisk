'use strict';

angular.module('app')
    .directive('uiFilms', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/directives/ui-genres/genres.tpl.html',
            scope: {
                genres: '=',
                actionId: '='
            }
        };
    });