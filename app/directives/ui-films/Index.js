'use strict';

angular.module('app')
    .directive('uiFilms', [function () {
        return {
            restrict: 'A',
            templateUrl: 'app/directives/ui-films/Films.tpl.html',
            scope: {
                uiFilms: '=uiFilms',
                cityID: '=cityId'
            },
            controller: ['$scope', '$compile', 'Film', function ($scope, $compile, Film) {
                // Переменная для сортировки
                $scope.reverse = false;

                // Если не был передан аргумент ид города
                if ($scope.cityID === undefined) {
                    $scope.cityID = 1;
                }

                // Установка дополнительного контента
                $scope.getTabs = function (cityID, filmID) {
                    var tabs = [
                        {
                            title: 'Подробнее о фильме',
                            loaded: false,
                            content: '',
                            onClick: function (scopeTab, $elementTab) {
                                if ( ! scopeTab.loaded) {
                                    Film.FindOne(filmID).then(function (film) {
                                        var contentTab = '';

                                        contentTab += '<p><b>Страна: </b>' + film.country + '</p>';
                                        contentTab += '<p><b>Время: </b>' + film.filmLength + '</p>';

                                        var creators = {
                                            director: [],
                                            actor: [],
                                            producer: []
                                        };
                                        angular.forEach(film.creators, function (items) {
                                            angular.forEach(items, function (item) {
                                                if (creators[item.professionKey] !== undefined) {
                                                    creators[item.professionKey].push(item.nameRU);
                                                }
                                            });
                                        });

                                        if (creators.director.length)
                                            contentTab += '<p><b>Режиссеры: </b>' + creators.director.join(', ') + '</p>';
                                        if (creators.actor.length)
                                            contentTab += '<p><b>Актеры: </b>' + creators.actor.join(', ') + '</p>';
                                        if (creators.producer.length)
                                            contentTab += '<p><b>Продюсеры: </b>' + creators.producer.join(', ') + '</p>';

                                        contentTab += '<p><b>Описание: </b>' + film.description + '</p>';

                                        // Заполнение контента таба
                                        $elementTab.append($compile(angular.element(contentTab))(scopeTab));
                                        scopeTab.loaded = true;
                                    });
                                }
                            }
                        },
                        {
                            title: 'Расписание сеансов',
                            loaded: false,
                            content: '',
                            onClick: function (scopeTab, $elementTab) {
                                if ( ! scopeTab.loaded) {
                                    Film.FindSeance($scope.cityID, filmID).then(function (film) {
                                        var contentTab = '';
                                        var len = film.items.length;

                                        angular.forEach(film.items, function (item, index) {
                                            var cinemaContent = '';
                                            var r = len >= 4 ? 4 : len;
                                            var c = index % r;

                                            if (c == 0) {
                                                cinemaContent += '<div class="row">';
                                            }
                                            cinemaContent += '<div class="col-lg-' + (12/r) + '">';
                                            // TODO:
                                            // cinemaContent += '<p>';
                                            cinemaContent += '<h4>' + item.cinemaName + '</h4>';
                                            // cinemaContent += '<a ng-click="onShowMap(\'' +item.cinemaName+'\',\''+item.address+'\','+item.lat+','+item.lon+ ')">Посмотреть на карте</a></p>'

                                            var seances = [];
                                            angular.forEach(item.seance, function (time) {
                                                seances.push(time);
                                            });
                                            angular.forEach(item.seance3D, function (time) {
                                                seances.push(time + ' (в 3D)');
                                            });

                                            var date = new Date();
                                            var h = date.getHours();
                                            var m = date.getMinutes();
                                            for (var i in seances.sort()) {
                                                var time = seances[i].match(/([0-9]*):([0-9]*)/);
                                                cinemaContent += '<p' + ( h >= time[1] || (h >= time[1] && m >= time[2]) ? ' class="ended"' : '') + '> - ' + seances[i] + '</p>'
                                            }

                                            cinemaContent += '</div>';
                                            if (c == r - 1 || index == len - 1) {
                                                cinemaContent += '</div>';
                                            }

                                            contentTab += cinemaContent;
                                        });

                                        // TODO:
                                        /*scopeTab.onShowMap = function (title, address, lat, lon) {
                                            var htmlModalMap = '<ui-modal title="' + title + '" address="' + address + '" lat="' + lat + '" lon="' + lon + '" />';
                                            var templateModalMap = angular.element(htmlModalMap);
                                            var linkFnModalMap = $compile(templateModalMap);

                                            $elementTab.append(linkFnModalMap(scopeTab));
                                        };*/

                                        $elementTab.append($compile(angular.element(contentTab))(scopeTab));
                                        scopeTab.loaded = true;
                                    });
                                }
                            }
                        }
                    ];

                    return tabs;
                };
            }]
        };
    }])
    .directive('uiTabs', function () {
        return {
            restrict: 'A',
            templateUrl: 'app/directives/ui-films/Tabs.tpl.html',
            scope: {
                uiTabs: '&'
            },
            controller: function ($scope, $element) {
                $scope.tabs = $scope.uiTabs();
                $scope.tabs.index = -1;
                $scope.onClickTab = function (index) {

                    if ($scope.tabs[index].onClick !== undefined && (typeof $scope.tabs[index].onClick == 'function')) {
                        var $elementTab = angular.element($element[0].querySelectorAll('.tab')[index]);
                        $scope.tabs[index].onClick($scope.tabs[index], $elementTab);
                    }

                    $scope.tabs.index = $scope.tabs.index != index ? index : -1
                }


            }
        };
    })/*
    TODO:
    .directive('uiModal', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/directives/ui-films/Modal.tpl.html',
            scope: {
                title: '@',
                address: '@',
                lat: '@',
                lon: '@'
            },
            controller: function ($scope, $element) {
                $scope.active = true;
            }
        };
    })*/;