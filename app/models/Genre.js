angular.module('app')
    .factory('Genre', ['$http', '$q', 'apiUrl', function($http, $q, apiUrl) {

        // Объявление модели
        var GenreModel = function (data) {
            if (data) {
                this.setData(data);
            }
        };

        // Пополнение списка
        GenreModel.prototype.setData = function (data) {
            angular.extend(this, data);
        };

        // Методы для получения жынров
        var Genre = {
            FindAll: function (cityID) {
                var deferred = $q.defer();
                var genres = [];
                var params = {};

                // Параметры фильтрации
                if (cityID !== undefined && cityID > -1) {
                    params.cityID = cityID;
                }

                // Обращение по API к кинопоиску
                $http.get(apiUrl + '/getGenres', {params: params})
                    .success(function(items) {

                        angular.forEach(items.genreData, function (data) {
                            genres.push(new GenreModel(data));
                        });

                        deferred.resolve(genres);
                    }).error(function () {
                        deferred.reject();
                    });

                return deferred.promise;
            }
        };

        return Genre;
    }]);