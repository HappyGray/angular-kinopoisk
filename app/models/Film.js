angular.module('app')
    .factory('Film', ['$http', '$q', 'apiUrl', function($http, $q, apiUrl) {

        // Объявление модели
        var FilmModel = function (data) {
            if (data) {
                this.setData(data);
            }
        };

        // Пополнение списка
        FilmModel.prototype.setData = function (data) {

            if (data.posterURL) {
                data.posterURL = 'http://www.kinopoisk.ru/images/' + data.posterURL;
            }
            if (data.genre) {
                data.genre = data.genre.split(', ');
            }
            if (data.rating !== undefined) {
                var match = data.rating.replace(/ /g, '').match(/(-?\d+\.\d{0,})\((\d*)\)/);

                data.rating = match[1]*1;
            }

            angular.extend(this, data);
        };

        // Методы для получения фильмов
        var Film = {
            // Получить фильмы в прокате
            FindToday: function (cityID, genreID) {
                var deferred = $q.defer();
                var films = [];
                var params = {};

                // Параметры фильтрации
                if (cityID !== undefined && cityID > -1) {
                    params.cityID = cityID;
                }
                if (genreID !== undefined && genreID > -1) {
                    params.genreID = genreID;
                }

                // Обращение по API к кинопоиску
                $http.get(apiUrl + '/getTodayFilms', {params: params})
                    .success(function(data) {

                        angular.forEach(data.filmsData, function (data) {
                            films.push(new FilmModel(data));
                        });

                        deferred.resolve(films);
                    })
                    .error(function () {
                        deferred.reject();
                    });

                return deferred.promise;
            },
            // Получить подробные данные о фильме
            FindOne: function (filmID) {
                var deferred = $q.defer();
                var params = {};

                if (filmID) {
                    params.filmID = filmID;

                    // Обращение по API к кинопоиску
                    $http.get(apiUrl + '/getFilm', {params: params})
                        .success(function(data) {
                            deferred.resolve(new FilmModel(data));
                        })
                        .error(function () {
                            deferred.reject();
                        });
                }

                return deferred.promise;
            },
            // Получить список сеансов по кинотеатру
            FindSeance: function (cityID, filmID) {
                var deferred = $q.defer();
                var params = {};

                if (cityID && filmID) {
                    // Параметры фильтрации
                    params.cityID = cityID;
                    params.filmID = filmID;

                    // Обращение по API к кинопоиску
                    $http.get(apiUrl + '/getSeance', {params: params})
                        .success(function(data) {
                           deferred.resolve(new FilmModel(data));
                        })
                        .error(function () {
                            deferred.reject();
                        });
                }

                return deferred.promise;
            }
        };

        return Film;
    }]);